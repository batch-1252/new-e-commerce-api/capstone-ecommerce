
const User = require("./../models/User");
const Product = require('./../models/Product');
const bcrypt = require('bcrypt');
const auth = require('./../auth');


module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email})
	.then( (result) => {
		if(result.length != 0){ 
			return true
		} else {
			return false
		}
	})
}

module.exports.register = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then( (result, error) =>{
		if(error){
			return error
		} else {
			return true
		}
	})
} 


module.exports.login = (reqBody) => { 

	return User.findOne({email: reqBody.email}).then( (result) => {

		if(result == null){
			return false

		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect === true){
				return { access: auth.createAccessToken(result.toObject())}
			} else {
				return false
			}
		}
	})
}


module.exports.getProfile = (data) => {
	console.log(data)

	return User.findById(data).then( result => {

		result.password = "******"
		return result
	})
}

module.exports.customer = async (data) => {


	const userSaveStatus = await User.findById(data.userId).then( user => {

		user.customer.push({productId: data.productId})

		return user.save().then( (user, error) => {
			if(error){
				return false
			} else {
				return true
			}
		})
	})


	const productSaveStatus = await Product.findById(data.productId).then( product => {
		product.customer.push({userId: data.userId})

		return product.save().then( (product, error) => {
			if(error){
				return false
			} else {
				return true
			}
		})
	})


	if(userSaveStatus && productSaveStatus){
		return true
	} else {
		return false
	}
}

module.exports.setUserAsAdmin = (userId)=>{
    return User.findByIdAndUpdate(userId, {isAdmin: true }, {new: true})
	    .then((result, error) => {
	        if(error){
	            return error
	        } else {
	            return result
	        }
	    })
}

module.exports.viewCart = (userId) => {
	return User.findById(userId).then(async user => {
		console.log(user)
		let cart = await user.placeOrder.map(async item => {
			let result = await Product.findById(item.productId)
			return {
				name: result.name,
				price: result.price,
				description: result.description,
				isActive: result.isActive,
				quantity: item.quantity,
				totalPrice: result.price * item.quantity,
				purchaseOn: item.purchaseOn
			}
		})
		return await Promise.all(cart)
	})
}

module.exports.addToCart = async (data) => {
	return await User.findById(data.userId).then(async user => {
		let placeOrder = user.placeOrder;
		let orderIndex = placeOrder.findIndex(item => item.productId === data.productId); 
		if(orderIndex !== -1) {
			user.placeOrder[orderIndex].quantity++;
		} else {
			user.placeOrder.push({
				productId: data.productId,
				quantity: 1,
			});
		}
		return user.save().then( (user, error) => {
			console.log(error)
			if(error){
				return false
			} else {
				return true
			}
		})
	})
}