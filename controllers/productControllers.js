const Product = require('./../models/Product')

module.exports.getAllActive = () => {

    return Product.find({isActive: true}).then( result => {
        
        return result
    })
}

module.exports.getAllProduct = () => {

    return Product.find().then( result => {
        return result
    })
}

module.exports.addProduct = (reqBody) => {


    let newProduct = new Product({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    })

    return newProduct.save().then( (product, error) => {
        if(error){
            return false
        } else {
            return true
        }
    })
}


module.exports.getSingleProduct = (params) => {

    return Product.findById(params.productId).then( product => {

        return product
    })

}


module.exports.editProduct = (params, reqBody) => {

    let updatedProduct = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    }

    return Product.findByIdAndUpdate(params, updatedProduct, {new: true})
    .then((result, error) => {
        if(error){
            return error
        } else {
            return result
        }
    })
}


module.exports.archiveProduct = (params) => {

    let updatedActiveProduct = {
        isActive: false
    }

    return Product.findByIdAndUpdate(params, updatedActiveProduct, {new: true})
    .then( (result, error) => {
        if(error){
            return false
        } else {
            return true
        }
    })
}


module.exports.unarchiveProduct = (params) => {

    let updatedActiveProduct = {
        isActive: true
    }

    return Product.findByIdAndUpdate(params, updatedActiveProduct, {new: true})
    .then( (result, error) => {
        if(error){
            return false
        } else {
            return true
        }
    })
}


module.exports.deleteProduct = (params) => {

    return Product.findByIdAndDelete(params).then( (result, error) => {
        if(error){
            return false
        } else {
            return true
        }
    })
}