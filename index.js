const express = require('express');
const mongoose = require('mongoose');
const PORT = process.env.PORT || 4000; 
const app = express();
const cors = require('cors');

let userRoutes = require("./routes/userRoutes");
let productRoutes = require("./routes/productRoutes");

app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

mongoose.connect("mongodb+srv://daliuz:517892021@cluster0.mlkat.mongodb.net/e-commerce?retryWrites=true&w=majority",
		{
			useNewUrlParser: true, 
			useUnifiedTopology: true
		})
.then(()=>console.log(`Connected to Database`))
.catch( (error)=> console.log(error))


app.use("/api/users", userRoutes);
app.use("/api/products", productRoutes);

app.listen(PORT, () => console.log(`Connected to Server at PORT ${PORT}`));